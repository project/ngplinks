<?php

/*
** dba.module:
**   Allows administrators direct access to their Drupal database.
** Written by Jeremy Andrews <jeremy@kerneltrap.org>, June 2004.
** PostgreSQL functionality provided by AAM <aam@ugpl.de>
*/


// standard Drupal functions
function ngplinks_perm() {
  return array('access urchin','access ngplinks','administer ngplinks');
}

function ngplinks_help($path, $arg) {
  switch ($path) {
    case 'admin/modules/ngplinks':
    case 'admin/modules#description':
      return t('Add your ngplinks admin link to the menu.');
    case 'admin/help#ngplinks':
      return t('<p>The ngplinks module allows you to add a link directly to ngplinks into your navigation menu.</p>');
  }
}

function ngplinks_menu() {
  $items = array();
  $access = user_access('access ngplinks') || user_access('administer ngplinks');

  $items['subscribe'] = array(
    'title' => 'Subscribe',
    'page callback' => 'ngplinks_launch',
    'page arguments' => array('subscribe'),
    'access arguments' => array('access content'),
  );
  $items['contribute'] = array(
    'title' => 'Contribute',
    'page callback' => 'ngplinks_launch',
    'page arguments' => array('contribute'),
    'access arguments' => array('access content'),
  );
  $items['volunteer'] = array(
    'title' => 'Volunteer',
    'page callback' => 'ngplinks_launch',
    'page arguments' => array('volunteer'),
    'access arguments' => array('access content'),
  );
  $items['admin/reports/urchin'] = array(
    'title' => 'Urchin log analysis',
    'description' => 'View detailed Urchin log satistics',
    'page callback' => 'ngplinks_launch',
    'page arguments' => array('urchin'),
    'access arguments' => array('access urchin'),
  );
  $items['admin/settings/ngplinks'] = array(
    'title' => 'NGP links',
    'description' => 'Configure links to NGP CWP forms and Urchin reports',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ngplinks_settings'),
    'access arguments' => array('administer site configuration'),
  );

  return $items;
}

function ngplinks_settings() {
  $form['ngplinks_urchin_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('NGP Urchin Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['ngplinks_urchin_settings']['ngplinks_urchin_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('ngplinks_urchin_username', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('Username for Urchin administrative login.'),
  );
  $form['ngplinks_urchin_settings']['ngplinks_urchin_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('ngplinks_urchin_password', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('Password for Urchin administrative login.'),
  );
  $form['ngplinks_urchin_settings']['ngplinks_urchin_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => variable_get('ngplinks_urchin_server', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('URL of Urchin login script.'),
  );
  /*$form['ngplinks_urchin_settings']['ngplinks_urchin_newwindow'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open in new window'),
    '#return_value' => 1,
    '#default_value' => variable_get('ngplinks_urchin_newwindow', 0),
    '#description' => t("Open the ngplinks admin in a new window."),
  );*/
  $form['ngplinks_cwp_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('NGP CWP Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['ngplinks_cwp_settings']['ngplinks_cwp_subscribe_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscribe URL'),
    '#default_value' => variable_get('ngplinks_cwp_subscribe_url', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('URL for CWP subscribtion page<br />Will be linked to '.l(url('subscribe', array('absolute' => TRUE)), 'subscribe')),
  );
  $form['ngplinks_cwp_settings']['ngplinks_cwp_contribute_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Contribute URL'),
    '#default_value' => variable_get('ngplinks_cwp_contribute_url', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('URL for CWP contribution page<br />Will be linked to '.l(url('contribute', array('absolute' => TRUE)), 'contribute')),
  );
  $form['ngplinks_cwp_settings']['ngplinks_cwp_volunteer_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Volunteer URL'),
    '#default_value' => variable_get('ngplinks_cwp_volunteer_url', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('URL for CWP volunteer page<br />Will be linked to '.l(url('volunteer', array('absolute' => TRUE)), 'volunteer')),
  );
  $form['ngplinks_cwp_settings']['ngplinks_cwp_theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme'),
    '#default_value' => variable_get('ngplinks_cwp_theme', ''),
    '#size' => 45,
    '#maxlength' => 255,
    '#description' => t('Default theme name'),
  );
  return system_settings_form($form);
}

function ngplinks_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks[0] = array('info' => t('Subscribe'), 'weight' => 0, 'enabled' => 0, 'region' => 'left');
    $blocks[1] = array('info' => t('Contribute'), 'weight' => 0, 'enabled' => 0, 'region' => 'left');
    return $blocks;
  }
  else if ($op == 'configure') {
    switch($delta) {
      case 0:
        $form['subscribe_inline'] = array(
          '#type' => 'checkbox',
          '#title' => t('Inline field titles'),
          '#return_value' => 1,
          '#default_value' => variable_get('ngplinks_subscribe_inline', 1),
          '#description' => t('Display the field titles inside the field.  They\'ll disappear when a user clicks into the field'),
        );
        $form['subscribe_name'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show name fields'),
          '#return_value' => 1,
          '#default_value' => variable_get('ngplinks_subscribe_name', 0),
          '#description' => t('Checking this will show the first and last name fields in the subscription block'),
        );
        $form['subscribe_zipcode'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show zipcode field'),
          '#return_value' => 1,
          '#default_value' => variable_get('ngplinks_subscribe_zipcode', 1),
          '#description' => t('Unchecking this will hide the zipcode field in the subscription block'),
        );
        break;
      case 1:
        $form['contribute_inline'] = array(
          '#type' => 'checkbox',
          '#title' => t('Inline field titles'),
          '#return_value' => 1,
          '#default_value' => variable_get('ngplinks_contribute_inline', 1),
          '#description' => t('Display the field titles inside the field.  They\'ll disappear when a user clicks into the field'),
        );
        break;
    }
    return $form;
  }
  else if ($op == 'save' && $delta == 0) {
    variable_set('ngplinks_subscribe_inline', $edit['subscribe_inline']);
    variable_set('ngplinks_subscribe_name', $edit['subscribe_name']);
    variable_set('ngplinks_subscribe_zipcode', $edit['subscribe_zipcode']);
    variable_set('ngplinks_block_items', $edit['items']);
  }
  else if ($op == 'view') {
    drupal_add_js(ngplinks_javascript(), 'inline', 'header');
    switch($delta) {
      case 0:
        $block = array('subject' => t('Subscribe'),
          'content' => drupal_get_form('ngplinks_form_subscribe'));
        break;
      case 1:
        $block = array('subject' => t('Contribute'),
          'content' => drupal_get_form('ngplinks_form_contribute'));
        break;
    }
    return $block;
  }
}

/**
 * Subscription Form
 */
function ngplinks_form_subscribe() {
  $form = array();

  if (variable_get('ngplinks_subscribe_name', 0)) {
    $form['first_name'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 256,
      '#description' => NULL,
      '#attributes' => NULL,
      '#required' => FALSE,
    );
    $form['last_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 256,
      '#description' => NULL,
      '#attributes' => NULL,
      '#required' => FALSE,
    );
    if (variable_get('ngplinks_subscribe_inline', 1)) {
      $form['first_name']['#title'] = '';
      $form['first_name']['#default_value'] = t('First Name');
      $form['first_name']['#attributes'] = array('onclick' => "if(this.value=='". t('First Name') ."'){this.value='';}");
      $form['last_name']['#title'] = '';
      $form['last_name']['#default_value'] = t('Last Name');
      $form['last_name']['#attributes'] = array('onclick' => "if(this.value=='". t('Last Name') ."'){this.value='';}");
    }
  }
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#default_value' => '',
    '#size' => 20,
    '#maxlength' => 256,
    '#description' => NULL,
    '#attributes' => NULL,
    '#required' => TRUE,
  );
  if (variable_get('ngplinks_subscribe_inline', 1)) {
    $form['email']['#title'] = '';
    $form['email']['#default_value'] = t('Email Address');
    $form['email']['#attributes'] = array('onclick' => "if(this.value=='". t('Email Address') ."'){this.value='';}");
  }
  if (variable_get('ngplinks_subscribe_zipcode', 1)) {
    $form['zipcode'] = array(
      '#type' => 'textfield',
      '#title' => t('Zip Code'),
      '#default_value' => '',
      '#size' => 7,
      '#maxlength' => 10,
      '#description' => NULL,
      '#attributes' => NULL,
      '#required' => FALSE,
    );
    if (variable_get('ngplinks_subscribe_inline', 1)) {
      $form['zipcode']['#title'] = '';
      $form['zipcode']['#default_value'] = t('Zip Code');
      $form['zipcode']['#attributes'] = array('onclick' => "if(this.value=='". t('Zip Code') ."'){this.value='';}");
    }
  }
  $form['submit-subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );

  return $form;
}

/**
 * Validate Subscription Form
 */
function ngplinks_form_subscribe_validate($form, &$form_state) {
  if (user_validate_mail($form_state['values']['email'])){
    form_set_error('email', t('Your Email address is invalid.'));
  }
}

/**
 * Submit Subscription Form
 */
function ngplinks_form_subscribe_submit($form, &$form_state) {
  if (variable_get('ngplinks_cwp_theme', '') != '') {
    $theme = '&m='.variable_get('ngplinks_cwp_theme', '');
  } else {
    $theme = '';
  }
  $url = variable_get('ngplinks_cwp_subscribe_url', '').$theme;
  if ($form_state['values']['first_name'] != '') {
    $url .= '&f='.$form_state['values']['first_name'];
  }
  if ($form_state['values']['last_name'] != '') {
    $url .= '&l='.$form_state['values']['last_name'];
  }
  if ($form_state['values']['email'] != '') {
    $url .= '&e='.$form_state['values']['email'];
  }
  if (($form_state['values']['zipcode'] != '') && ($form_state['values']['zipcode'] != t('Zip Code'))) {
    $url .= '&z='.$form_state['values']['zipcode'];
  }
  header('Location: '.$url);
  exit;
}

/**
 * Contribution Form
 */
function ngplinks_form_contribute() {
  $form = array();

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => '',
    '#size' => 7,
    '#maxlength' => 256,
    '#description' => NULL,
    '#attributes' => NULL,
    '#required' => TRUE,
  );
  if (variable_get('ngplinks_subscribe_inline', 1)) {
    $form['amount']['#title'] = '';
    $form['amount']['#default_value'] = t('Amount');
    $form['amount']['#attributes'] = array('onclick' => "if(this.value=='". t('Amount') ."'){this.value='';}");
  }
  $form['submit-contribute'] = array(
    '#type' => 'submit',
    '#value' => t('Contribute'),
  );

  return $form;
}

/**
 * Submit Subscription Form
 */
function ngplinks_form_contribute_submit($form, &$form_state) {
  if (variable_get('ngplinks_cwp_theme', '') != '') {
    $theme = '&m='.variable_get('ngplinks_cwp_theme', '');
  } else {
    $theme = '';
  }
  $url = variable_get('ngplinks_cwp_contribute_url', '').$theme;
  if (($form_state['values']['amount'] != '') && ($form_state['values']['amount'] != t('Amount'))) {
    $url .= '&a='.$form_state['values']['amount'];
  }
  header('Location: '.$url);
  exit;
}

function ngplinks_launch($link) {
  if ($link == 'urchin') {
    if (variable_get('ngplinks_urchin_username', '') != '' && variable_get('ngplinks_urchin_password', '') != '' && variable_get('ngplinks_urchin_server', '') != '') {
      //if (variable_get('ngplinks_newwindow', 0) == 0) {
        drupal_set_header('Location: '.variable_get('ngplinks_urchin_server', '').'?user='.variable_get('ngplinks_urchin_username', '').'&pass='.variable_get('ngplinks_urchin_password', '').'&app=admin&action=login');
      //} else {
      //  drupal_set_header('Location: '.$_SERVER['HTTP_REFERER']);
      //}
    } else {
      drupal_not_found();
    }
  } else {
    if (variable_get('ngplinks_cwp_'.$link.'_url', '') != '') {
      if (variable_get('ngplinks_cwp_theme', '') != '') {
        $theme = '&m='.variable_get('ngplinks_cwp_theme', '');
      } else {
        $theme = '';
      }
      drupal_set_header('Location: '.variable_get('ngplinks_cwp_'.$link.'_url', '').$theme);
    } else {
      drupal_not_found();
    }
  }
}

function ngplinks_javascript() {
  if (variable_get('ngplinks_cwp_theme', '') != '') {
    $theme = '&m='.variable_get('ngplinks_cwp_theme', '');
  } else {
    $theme = '';
  }
  $subscribe = variable_get('ngplinks_cwp_subscribe_url', '').$theme;
  $contribute = variable_get('ngplinks_cwp_contribute_url', '').$theme;
  $volunteer = variable_get('ngplinks_cwp_volunteer_url', '').$theme;

  $ngplinks_javascript = <<<EOD

userAgent = window.navigator.userAgent;
browserVers = parseInt(userAgent.charAt(userAgent.indexOf("/")+1),10);

function loadEmail() {
  var u = '$subscribe';
  var e = document.getElementById("email");
  if (!(e==null)){
  	var ev = e.value;
  	if (!(ev=="") && (validateEmail(ev))) u=u+"&e="+ev;
  }
  var z = document.getElementById("zipcode");
  if (!(z==null)){
  	var zv = z.value;
  	if (!(zv=="") && (validateZip(zv))) u=u+"&z="+zv;
  }
  setCookie('splash_skip','TRUE',30);
  document.location.href=u;
  return false;
}

function loadContrib() {
  var u = '$contribute';
  var f = document.getElementById("amount");
  if (!(f==null)){
    var fg = f.value;
    if (!(isNaN(fg)) && !(fg=="")) u=u+"&a="+fg;
  }
  setCookie('splash_skip','TRUE',120);
  document.location.href=u;
  return false;
}

function loadVolunteer() {
  var u = '$volunteer';
  setCookie('splash_skip','TRUE',365);
  document.location.href=u;
  return false;
}

function validateZip(field) {
  var valid = "0123456789";
  var hyphencount = 0;
  if (field.length!=5) return false;
  for (var i=0; i < field.length; i++) {
    temp = "" + field.substring(i, i+1);
    if (valid.indexOf(temp) == "-1") return false;
  }
  return true;
}

function validateEmail(str) {
  var at="@";
  var dot=".";
  var lat=str.indexOf(at);
  var lstr=str.length;
  var ldot=str.indexOf(dot);
  if (str.indexOf(at)==-1) return false;
  if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr) return false;
  if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr) return false;
  if (str.indexOf(at,(lat+1))!=-1) return false;
  if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot) return false;
  if (str.indexOf(dot,(lat+2))==-1) return false;
  if (str.indexOf(" ")!=-1) return false;
  return true;
}

function setCookie(cookieName,cookieValue,nDays) {
  var today = new Date();
  var expire = new Date();
  if (nDays==null || nDays==0) nDays=1;
  expire.setTime(today.getTime() + 3600000*24*nDays);
  document.cookie = cookieName+"="+escape(cookieValue)+";expires="+expire.toGMTString();
}

function clearDefault(el) {
  el.value = "";
}

EOD;

  return $ngplinks_javascript;
}
